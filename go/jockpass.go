package main

import (
	"io/ioutil"
	"log"
	"math/rand"
	"os"
	"sort"
	"strconv"
	"time"
)

const (
	POPULATION_SIZE = 100
	GENES           = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ 1234567890,.-;:_!\"#%&/()=?@${[]}"
)

var TARGET = ""

type Individual struct {
	Chromesome string
	Fitness    int
}

func NewIndividual(chromesome string) *Individual {
	in := &Individual{Chromesome: chromesome}
	in.Fitness = in.CalcFitness()

	return in
}

func (in *Individual) Mate(partner Individual) *Individual {
	child := ""
	l := len(in.Chromesome)

	for i := 0; i < l; i++ {
		p := rand.Float64() / 1.0
		if p < 0.45 {
			child += string(in.Chromesome[i])
		} else if p < 0.9 {
			child += string(partner.Chromesome[i])
		} else {
			child += mutatedGene()
		}
	}

	return NewIndividual(child)
}

func (in *Individual) CalcFitness() int {
	len := len(TARGET)
	fitness := 0

	for i := 0; i < len; i++ {
		if in.Chromesome[i] != TARGET[i] {
			fitness++
		}
	}

	return fitness
}

func createGenome() string {
	l := len(TARGET)
	genome := ""

	for i := 0; i < l; i++ {
		genome += mutatedGene()
	}

	return genome
}

func mutatedGene() string {
	l := len(GENES)
	rnd := rand.Intn(l - 1)

	return string(GENES[rnd])
}

func main() {
	args := os.Args

	if len(args) < 3 {
		log.Fatalln("Usage: " + args[0] + " {pass} {fitness}")
	}

	TARGET = args[1]
	desiredFitness, err := strconv.Atoi(args[2])
	if err != nil {
		log.Fatalln("\"" + args[2] + "\" is not an integer")
	}

	rand.Seed(time.Now().UnixNano())

	population := []*Individual{}

	for i := 0; i < POPULATION_SIZE; i++ {
		genome := createGenome()
		population = append(population, NewIndividual(genome))
	}

	generation := 0
	candidates := ""
	for {
		sort.SliceStable(population, func(i, j int) bool {
			return population[i].Fitness < population[j].Fitness
		})

		if population[0].Fitness <= 0 {
			break
		}

		newGeneration := []*Individual{}
		s := (10 * POPULATION_SIZE) / 100
		for i := 0; i < s; i++ {
			newGeneration = append(newGeneration, population[i])
		}

		s = (90 * POPULATION_SIZE) / 100
		for i := 0; i < s; i++ {
			l := len(population)
			rnd := rand.Intn(l / 2)
			parent1 := population[rnd]

			rnd = rand.Intn(l / 2)
			parent2 := population[rnd]

			child := parent1.Mate(*parent2)
			newGeneration = append(newGeneration, child)
		}

		population = newGeneration
		best := population[0]
		log.Printf("Generation: %d\tGuess: %s\tFitness: %d\n", generation, best.Chromesome, best.Fitness)

		if best.Fitness == desiredFitness {
			candidates += best.Chromesome + "\n"
		}

		generation++
	}

	best := population[0]
	log.Printf("Generation: %d\tGuess: %s\tFitness: %d\n", generation, best.Chromesome, best.Fitness)

	if err := ioutil.WriteFile("candidates.txt", []byte(candidates), 0644); err != nil {
		log.Fatalln(err)
	}
}
